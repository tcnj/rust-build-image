FROM rust:1-bullseye

ADD . /

RUN set -x; \
    mkdir -p /root/.cargo; \
    cp /cargo-config /root/.cargo/config; \
    for toolchain in `cat /toolchains`; do \
        rustup toolchain install $toolchain; \
    done; \
    for target in "`cat /targets`"; do \
        for toolchain in `cat /toolchains`; do \
            rustup target add --toolchain $toolchain $target; \
        done; \
    done; \
    for tool in "`cat /tools`"; do \
        cargo install $tool; \
    done
